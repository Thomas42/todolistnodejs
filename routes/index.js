var express = require('express');
var router = express.Router();
var Model = require("../models/model");

/* GET home page. */
router.get('/', function (req, res) {
  const model = new Model();

    model.getAll(function(data) {
        res.render('index', {title: 'Home - TodoList', tasks: data.tasks, users: data.users});
    });
});

// Post : new task
router.post('/', function (req, res) {
  const model = new Model();

  model.addTask(req.body.newTaskName, req.body.userControlSelect, function () {
      res.redirect('/');
  }, function (err) {
      model.getAll(function(data) {
          res.render('index', {title: 'Home - TodoList', tasks: data.tasks, users: data.users, error: err});
      });
  });

});
module.exports = router;
