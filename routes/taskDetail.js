var express = require('express');
var router = express.Router();

var Model = require("../models/model");

/* GET users listing. */
router.get('/:detailId', function (req, res, next) {
  console.log(req.params);
    const model = new Model();
    model.getTask(req.params.detailId, function(task) {
        res.render('detail.twig', {title: task.name + " - TodoList", task: task});
    });
});

module.exports = router;
