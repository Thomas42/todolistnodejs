var config = {
    development: {
        url: 'http://localhost:8081/',

        //server details
        server: {
            host: '127.0.0.1',
            port: '8081'
        },
        api: {
            host: 'http://127.0.0.1',
            port: '8080'
        }
    }
    /*,
    production: {
       url: '',

        //server details
        server: {
            host: '',
            port: ''
        },
        api: {
            host: '',
            port: ''
        }
    }*/
};
module.exports = config;