# TodoList client
GraphQL client demo

## Install
* `git clone https://bitbucket.org/Thomas42/todolistnodejs.git` 
* `npm install` 
* Create `config.js` like `config.default.js` template
* Install GulpJS : `npm install -g gulp`

## Run
`gulp`  
 
Server is auto restart when your modify it.