var gulp = require('gulp');
var nodemon = require('gulp-nodemon');

gulp.task('default', function () {

    let env = process.env.NODE_ENV || 'development';
    let config = require('./config')[env];

    const port = config.server.port || process.env.PORT || 3001;

  nodemon({
    script: './bin/www'
    , ext: 'js html twig'
    , env: {
      'NODE_ENV': 'development',
      'PORT': port
    }
  })
});