const ApolloClient = require('apollo-client');
const Cache = require('apollo-cache-inmemory');
const fetch = require('node-fetch');
const HttpLink = require('apollo-link-http');
const gql = require('graphql-tag');
const lodash = require('lodash');

function Model() {
    let env = process.env.NODE_ENV || 'development';
    let config = require('../config')[env];

    this.client = new ApolloClient.ApolloClient({
        link: new HttpLink.HttpLink({ uri: config.api.host+":"+config.api.port, fetch: fetch }),
        cache: new Cache.InMemoryCache()
    });

}

// class methods

/*
    get all
        *   tasks
        *   users
 */
Model.prototype.getAll = function (success) {
    this.client.query({
        query: gql`
    query {
      tasks {
        id,
        name,
        user {
            username
        }
      },
       users {
        id,
        username
       }
     }
  `,
    })
        .then(data => success(data.data))
        .catch(error => console.error(error));
};

// Create new task
Model.prototype.addTask = function (name, userId, success, error) {
    // check task name
    if(name === undefined || name === "") {
        var err = "no name to create task";
        console.log("TasksModel.add ERROR "+err);
        if(error !== undefined) {
            error(err, this.getAll());
        }
        return;
    }
    // check user id is set
    if(userId === undefined || userId === "") {
        var err = "no user select to create task";
        console.log("TasksModel.add ERROR "+err);
        if(error !== undefined) {
            error(err, this.getAll());
        }
        return;
    }

    var currentDate = new Date().toISOString().slice(0,10);

    this.client.mutate({
        variables: {
            "name":name,
            "date": currentDate,
            "userId": userId
        },
        mutation: gql`
        mutation($name:String!, $date: String!, $userId:String!) {
            newTask(task: {
              name: $name,
              date: $date,
              userId: $userId
            }) {
              id
            }
        } 
  `,
    })
        .then(data => success() )
        .catch(errorMessage => error(errorMessage.message));
};

/**
 * get a task
 * @param id
 * @param success
 */
Model.prototype.getTask = function (id, success) {
    this.getAll(function (data) {
        const res = lodash.filter(data.tasks, x => x.id === id);
        success(res[0]);
    });

};


// export the class
module.exports = Model;